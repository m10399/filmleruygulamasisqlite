package com.example.filmleruygulamasi

import android.annotation.SuppressLint


@SuppressLint("Range")
class FilmlerDao {
    fun tumFilmlerByKategoriId(vt:VeritabaniYardimcisi,kategori_id:Int):ArrayList<Fimler>{
        val db = vt.writableDatabase
        val FilmlerListe = ArrayList<Fimler>()

        val c = db.rawQuery("SELECT * FROM filmler,kategoriler,yonetmenler where filmler.kategori_id = kategoriler.kategori_id and filmler.yonetmen_id = yonetmenler.yonetmen_id and kategoriler.kategori_id = ${kategori_id}",null)
        while (c.moveToNext()){

            val kategori = Kategoriler(c.getInt(c.getColumnIndex("kategori_id")),
                c.getString(c.getColumnIndex("kategori_ad")))

            val yonetmen = Yonetmenler(c.getInt(c.getColumnIndex("yonetmen_id")),
                c.getString(c.getColumnIndex("yonetmen_ad")))

            val film = Fimler(c.getInt(c.getColumnIndex("film_id")),
                c.getString(c.getColumnIndex("film_ad")),
                c.getInt(c.getColumnIndex("film_yil")),
                c.getString(c.getColumnIndex("film_resim")),
                kategori,yonetmen
                )
            FilmlerListe.add(film)
        }
        return FilmlerListe
    }
}